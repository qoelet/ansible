# -*- mode: ruby -*-
# vi: set ft=ruby :

# We want to emulate the SSD drive that's mounted on our PoPs for edge content
ssd = 'pop-ssd.vdi'
controller_name = 'SCSI'

Vagrant.configure(2) do |config|
  config.vm.network "public_network",
    bridge: [
      "en0: Wi-Fi (AirPort)", # MacBook
      "en0: Ethernet", # iMac
      "en6: Broadcom NetXtreme Gigabit Ethernet Controller",
    ],
    use_dhcp_assigned_default_route: true
    # config.vm.box = "ubuntu/xenial64" # 16.04
    config.vm.box = "ubuntu/bionic64" # 18.04
  config.vm.provider "virtualbox" do |vb|
    vb.customize [
      "modifyvm", :id,
      "--name", "Commons"]
    vb.gui = false
    vb.memory = "2048"
    unless File.exist?(ssd)
      vb.customize [
        'createhd',
        '--filename', ssd,
        '--variant', 'Fixed',
        '--size', 8 * 1024
      ]
    end
    vb.customize [
      'storageattach', :id,
      '--storagectl', controller_name,
      '--port', 2, '--device', 0,
      '--type', 'hdd', '--medium', ssd,
      '--nonrotational', 'on',
      '--discard', 'on'
    ]
  end
  config.trigger.after :halt do |trigger|
    trigger.info =  "Detach SSD"
    trigger.run = {
      inline: "VBoxManage storageattach Commons --storagectl 'SCSI' --port 2 --device 0 --type hdd --medium none"
    }
  end
  config.vm.provision "ansible" do |ansible|
    ansible.playbook = "vagrant.yaml"
  end
  config.ssh.forward_agent = true
end
